#!/usr/bin/env python3

import asyncio
import aiohttp
import calendar
import datetime
import dateutil.rrule
import os
import argparse


async def fetch_goes_data(session, base_path='_cache',
                          satellite=6, date=datetime.date(1987, 1, 1)):
    '''Fetch GOES satellite data for a particular date and satellite.

    We use the 5-minute averaged monthly streams from NOAA's official
    repository.  To be polite to their web servers (after all, this is
    a big data set!), this function maintains a local on-disk cache of
    previous fetches.'''

    # Ensure the cache path exists.
    if not os.path.exists(base_path):
        os.mkdir(base_path)

    # Convert arguments into an URI on the NOAA server corresponding to
    # the CSV file we want.
    (y, m, d) = (date.year, date.month, date.day)
    d2 = calendar.monthrange(date.year, date.month)[1]

    uri = r'http://satdat.ngdc.noaa.gov/sem/goes/data/avg/'      \
        '{y}/{m:02d}/goes{sat:02d}/csv/'                         \
        'g{sat:02d}_eps_5m_{y}{m:02d}{d:02d}_{y}{m:02d}{d2:02d}.csv'.format(
            y=y, m=m, d=d, d2=d2, sat=satellite)

    # Now, we need to determine if a local cache exists. We use the file
    # portion of the URI since it's already an unique, legal filename.
    filename = os.path.join(base_path, uri.split('/')[-1])

    # If the file exists, use it. It either contains valid CSV data, or is
    # an 0-length marker file indicating a null result (presumably the
    # requested satellite does not have archived results on the requested
    # date.)
    if os.path.exists(filename):
        data = open(filename).read()
        return data if data else None

    # No cached results available. Fetch!
    with open(filename, 'w') as f:
        response = await session.get(uri)
        if response.status == 200:
            # Successful response. Update the cache and return what we got.
            data = await response.text()
            f.write(data)
            return data
        else:
            # Data did not exist -- deliberately leave behind an empty file to
            # cache this result too.
            return None


async def main(dates, satellites):
    '''Attempt to fetch a large range of monthly GOES satellite data.'''

    connector = aiohttp.TCPConnector(limit=10)  # be polite-ish

    async with aiohttp.ClientSession(connector=connector) as session:

        futures = [
            asyncio.ensure_future(fetch_goes_data(
                session=session,
                satellite=satellite, date=date))
            for date in dates
            for satellite in satellites
        ]

        responses = await asyncio.gather(*futures)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description='Fetch GOES satellite data and place it in a local cache')

    parser.add_argument(
        '-f', '--from-date', dest='from_date',
        help='First year and month to fetch (default: 1989-11)',
        default=datetime.date(1989, 1, 1),
        type=lambda x: datetime.datetime.strptime(x, '%Y-%m'))

    parser.add_argument(
        '-t', '--to-date', dest='to_date',
        help='Last year and month to fetch (default: 1989-12)',
        default=datetime.date(1989, 12, 1),
        type=lambda x: datetime.datetime.strptime(x, '%Y-%m'))

    parser.add_argument(
        '-s', '--satellite', dest='satellite',
        help='GOES satellite IDs to fetch (default: 6)',
        type=int, nargs='+', default=[6])

    args = parser.parse_args()

    dates = dateutil.rrule.rrule(
        dateutil.rrule.MONTHLY,
        dtstart=args.from_date,
        until=args.to_date)

    loop = asyncio.get_event_loop()
    loop.run_until_complete(main(dates, args.satellite))
