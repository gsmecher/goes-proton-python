import csv
import datetime
import re
import collections


class GOESDictReader(csv.DictReader):
    __meta = {}
    __global_attributes = {}

    def __init__(self, f, *args, **kwds):

        class DefaultFormatter(object):
            missing_value = None
            format = None

            def __call__(self, value):
                if self.missing_value == float(value):
                    return None

                if self.format[0] in ('E', 'F'):
                    return float(value)

                return value

        self.formatters = {}
        var_decl_re = re.compile('(double|float) ([0-9A-Za-z_]*)\(([a-zA-Z0-9_]*)\);')
        var_tag_re = re.compile('([0-9A-Za-z_]*):([0-9A-Za-z_]*) = "(.*)";$')
        attribute_re = re.compile(':([0-9A-Za-z_-]*) = "(.*)"')

        # Skip to variables block
        for x in f:
            if x.strip() == 'variables:':
                break

        # Consume the variables block
        for x in f:
            if x.strip() == '// global attributes':
                break

            v = var_decl_re.match(x)
            if v:
                (type_, name, scope) = v.groups()
                self.formatters[name] = DefaultFormatter()
                self.__meta[name] = {}
                continue

            v = var_tag_re.match(x)
            if v:
                (name, key, value) = v.groups()
                self.__meta[name][key] = value

                if key == 'missing_value':
                    self.formatters[name].missing_value = float(value)
                elif key == 'format':
                    self.formatters[name].format = value

        # Consume the global attributes block
        for x in f:
            attr_match = attribute_re.match(x.strip())
            if attr_match:
                (k, v) = attr_match.groups()
                self.__global_attributes[k] = v
            else:
                break

        # Skip to data block
        for x in f:
            if x.strip() == 'data:':
                break

        # Datetime is not adequately captured in the above; set up a formatter
        # explicitly.
        def time_tag_formatter(x):
            # Parse timestamp into DateTime
            (dt, ms) = x.split('.')
            dt = datetime.datetime.strptime(dt, "%Y-%m-%d %H:%M:%S")
            ms = datetime.timedelta(milliseconds=int(ms))
            return dt + ms

        self.formatters['time_tag'] = time_tag_formatter

        super().__init__(f, *args, **kwds)

    def __next__(self):
        line = super().__next__()

        return collections.OrderedDict(
            (k, self.formatters[k](v)) for (k, v) in line.items())

    def get_global_attribute(self, key):
        return self.__global_attributes[key]

    def get_metadata(self):
        return self.__meta
