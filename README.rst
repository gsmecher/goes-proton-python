Retrieve and Analyze Proton Data from GOES Satellites
-----------------------------------------------------

This code accompanies a longer-form article available at
http://threespeedlogic.com.  You should be able to fetch and plot GOES proton
data simply by running "fetch.py" followed by "analyze.py"; the resulting plots
are placed in a "png" directory.

Requirements:

- Python 3.5 or newer
- The asyncio and aiohttp packages
- The numpy, scipy, and matplotlib packages
- A reasonably fast connection to NOAA's GOES data archive.

Enjoy!
