#!/usr/bin/python3

import calendar
import matplotlib.pyplot as plt
import numpy as np
import re
import glob
import sys
import os
import multiprocessing
import goes
import datetime


def plot(input_filename):

    # Parse the CSV data into a DictReader-like structure.
    gr = goes.GOESDictReader(open(input_filename))
    satellite_id = gr.get_global_attribute('satellite_id')
    start_date = datetime.datetime.strptime(
        gr.get_global_attribute('start_date'),
        '%Y-%m-%d %H:%M:%S.%f %Z')  # %Z is incorrect but irrelevant units
    year, month = start_date.year, start_date.month

    # Transpose the month's dataset from a list of dictionaries
    # to a dictionary of lists
    dat = {}
    for e in gr:
        for k, v in e.items():
            dat.setdefault(k, []).append(v)

    # Depending on what data series are present in the CSV file, we may
    # generate one or two plot. Define a plot function to use later.
    def do_plot(channel_template, legend, title, filename):
        plt.figure()
        cycler = plt.rcParams['axes.prop_cycle']()

        for n in range(1, 8):
            k = channel_template % n
            color = cycler.__next__()['color']

            x = np.array(range(len(dat[k])))
            plt.semilogy(x, dat[k], color=color)

        plt.ylim([1e-4, 2e5])
        plt.grid()
        plt.legend(legend, title='MeV Range (Min - Max)')
        tick_x = np.linspace(0, len(dat['time_tag'])-1,
                             num=len(plt.xticks()[0]),
                             endpoint=True, dtype=np.int)
        tick_y = [dat['time_tag'][x].strftime('%m-%d %H:%M') for x in tick_x]
        plt.xticks(tick_x, tick_y, rotation=15, ha='right')

        plt.ylabel(r'''
            Flux $\left[
                \mathrm{nuc} / \left(
                    \mathrm{cm}^2 \cdot \mathrm{s}
                    \cdot \mathrm{sr} \cdot \mathrm{MeV}
                \right)
            \right]$'''.replace('\n', ''))

        plt.suptitle(title)
        plt.tight_layout(rect=[0, 0.03, 1, 0.95])
        plt.savefig('png/%s.png' % os.path.splitext(filename)[0])
        plt.close()

    # If corrected proton channels are present in the CSV, generate a corrected
    # plot. This is more accurate but cannot be directly compared to non-corrected
    # datasets.
    desc_re = re.compile('P. Proton channel (.*) MeV')
    m = gr.get_metadata()
    if 'p1_flux_c' in m:
        legend = [desc_re.match(m['p%i_flux_c' % channel]['description']).group(1)
                  for channel in range(1, 8)]

        do_plot('p%i_flux_c', legend,
                '%s Flux, %s %i (Corrected)' % (satellite_id, calendar.month_abbr[month], year),
                '%s_corrected' % os.path.splitext(os.path.basename(input_filename))[0])

    # Produce a plot using uncorrected proton channels.
    legend = [desc_re.match(m['p%i_flux' % channel]['description']).group(1)
              for channel in range(1, 8)]
    do_plot('p%i_flux', legend,
            '%s Flux, %s %i (Uncorrected)' % (satellite_id, calendar.month_abbr[month], year),
            os.path.basename(input_filename))


if __name__ == '__main__':
    if not os.path.exists('png'):
        os.mkdir('png')

    # Find all cached GOES CSV files
    filenames = glob.iglob(sys.argv[1] if len(sys.argv) == 2 else '_cache/*')

    # Filter out the empty ones (they indicate "no valid data")
    filenames = (fn for fn in filenames if os.path.getsize(fn))

    # Process in a gigantic parallel execution. We need to use multiprocessing
    # here because matplotlib does not like threading.
    multiprocessing.Pool().map(plot, filenames)
